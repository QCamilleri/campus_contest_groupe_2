campus context

nous devons realiser pour M.N'Guyen Aux clients de se connecter et réserver des ouvrages en ligne (gestion du délai de mise à disposition) ;
 A lui et à son équipe de suivre les retraits/emprunts des ouvrages suite à une réservation en ligne ou non ;
 À lui et son équipe de suivre en temps réel les entrées et les sorties ;
 À lui et son équipe d’accéder à la base de données clients, et y apporter des modifications si besoin. ;
 À lui et à son équipe de relancer automatiquement par mail les clients n’ayant pas ramené leur ouvrage au-delà d’un certain délai. 
 
Bonus : pour aller plus loin 
 
 À lui et à son équipe de gérer les abonnements annuels, mensuels ou à la carte. 
 La société « Manga ++ » propose plusieurs types abonnements (annuels et mensuels) donnant droit à une quantité de livres empruntés.
 L’abonnement à la carte permet d’emprunter à l’unité avec la gestion d’une carte de fidélité.
 L’ensemble des abonnements doit être paramétrable par M. N’Guyen. 
 
 
 pour accéder au site fini :  http://jeremyb86.alwaysdata.net/index.php